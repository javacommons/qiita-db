package db.qiita;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "items")
public class Item {

    @DatabaseField(id = true)
    public String url;

    @DatabaseField(dataType = DataType.LONG_STRING)
    public String title;

    @DatabaseField(index = true)
    public String key;

    @DatabaseField()
    public Integer page;

    @DatabaseField(index = true)
    public String batch;

    @DatabaseField(index = true)
    public String created;

    @DatabaseField(dataType = DataType.LONG_STRING)
    public String icon;

    @DatabaseField(index = true)
    public Integer like;

    @DatabaseField(dataType = DataType.LONG_STRING)
    public String tags;

    @DatabaseField()
    public String user;

    @DatabaseField(index = true)
    public String ts;

    Item() {
        // all persisted classes must define a no-arg constructor with at least package visibility
    }

    public Item(String url) {
        this.url = url;
    }

    @Override
    public int hashCode() {
        return url.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null || other.getClass() != getClass()) {
            return false;
        }
        return url.equals(((Item) other).url);
    }
}
