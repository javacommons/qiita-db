package db.qiita;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "batches")
public class Batch {

    @DatabaseField(id = true)
    public String batch;

    @DatabaseField()
    public Integer count;

    //@DatabaseField(dataType = DataType.LONG_STRING)
    @DatabaseField(columnDefinition = "LONGTEXT")
    public String json;

    @DatabaseField(index = true)
    public String ts;

    Batch() {
        // all persisted classes must define a no-arg constructor with at least package visibility
    }

    public Batch(String batch) {
        this.batch = batch;
    }

    @Override
    public int hashCode() {
        return batch.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null || other.getClass() != getClass()) {
            return false;
        }
        return batch.equals(((Item) other).batch);
    }
}
