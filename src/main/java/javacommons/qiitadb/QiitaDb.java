package javacommons.qiitadb;

import com.gitlab.javacommons.paizadb.PaizaDb;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.logger.Level;
import com.j256.ormlite.logger.Logger;
import com.j256.ormlite.table.TableUtils;
import db.qiita.Batch;
import db.qiita.Item;

import java.sql.SQLException;

public class QiitaDb {
    static {
        try {
            Logger.setGlobalLogLevel(Level.ERROR);
            //JdbcConnectionSource connectionSource = QiitaDb.getJdbcConnectionSource();
            //TableUtils.createTableIfNotExists(connectionSource, Item.class);
            //TableUtils.createTableIfNotExists(connectionSource, Batch.class);
        } catch (Exception ex) {
        }
    }

    private static void prepareTables(JdbcConnectionSource connectionSource) {
        try {
            TableUtils.createTableIfNotExists(connectionSource, Item.class);
        } catch (SQLException e) {
        }
        try {
            TableUtils.createTableIfNotExists(connectionSource, Batch.class);
        } catch (SQLException e) {
        }
    }

    public static JdbcConnectionSource getJdbcConnectionSource(PaizaDb.DbType dbType) throws SQLException {
        JdbcConnectionSource connectionSource = PaizaDb.getJdbcConnectionSource(dbType, "qiita2");
        prepareTables(connectionSource);
        return connectionSource;
    }
}
